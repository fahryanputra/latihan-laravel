<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>SanberBook Sign Up</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
      @csrf
      
      <label for="fname">First name:</label><br><br>
      <input type="text" name="fname" id="fname">
      <br><br><br>

      <label for="lname">Last name:</label><br><br>
      <input type="text" name="lname" id="lname">
      <br><br><br>

      <p>Gender:</p>
      <input type="radio" id="male" name="gender" value="male">
      <label for="male">Male</label><br>
      <input type="radio" id="female" name="gender" value="female">
      <label for="female">Female</label><br>
      <input type="radio" id="genderother" name="gender" value="other">
      <label for="genderother">Other</label>
      <br><br><br>

      <label for="nationality">Nationality:</label><br><br>
      <select id="nationality" name="nationality">
        <option value="indonesia">Indonesian</option>
        <option value="japanese">Japanese</option>
        <option value="australian">Australian</option>
      </select>
      <br><br><br>

      <p>Spoken Language</p>
      <input type="checkbox" id="bahasa" name="lang" value="Bahasa Indonesia">
      <label for="bahasa">Bahasa Indonesia</label><br>
      <input type="checkbox" id="english" name="lang" value="English">
      <label for="english">English</label><br>
      <input type="checkbox" id="langother" name="lang" value="Other">
      <label for="langother">Other</label>
      <br><br><br>

      <label for="bio">Bio:</label><br><br>
      <textarea id="bio" name="bio" rows="8" cols="40"></textarea>
      <br><br><br>

      <input type="submit" value="Sign Up">
    </form>
  </body>
</html>
